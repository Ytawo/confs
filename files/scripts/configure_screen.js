#!/usr/bin/env node

const child_process = require('child_process');

const REGEXP = /([\w-]+) connected (primary )?\d+x\d+\+\d+\+\d+ \([\w ]+\) (\d+)mm x (\d+)mm/g;

function exec(command) {
    return child_process.execSync(command);
}

function main() {
    let screens = getScreenList();
    let hdmiScreen = screens.find(screen => screen.name.includes('HDMI'));
    let primaryScreen = screens.find(screen => screen.primary);

    if (hdmiScreen && hdmiScreen !== primaryScreen) {
        exec(`xrandr --output ${hdmiScreen.name} --mode ${hdmiScreen.resolution} --same-as ${primaryScreen.name}`);
    } else {
        exec(`xrandr --output ${primaryScreen.name} --mode ${primaryScreen.resolution}`);
    }

    exec(`feh --bg-scale ~/.wallpaper.png`);
}

function getResolutionFromSize(width, height) {
    let resolution = '1920x1080';

    if (width <= 300 && height <= 170) {
        resolution = '1368x768';
    }

    return resolution;
}

function getScreenList() {
    let output = exec('xrandr').toString();
    let result = [];
    
    for (let match of output.matchAll(REGEXP)) {
        let name = match[1];
        let primary = !!match[2];
        let width = parseInt(match[3]);
        let height = parseInt(match[4]);
        let resolution = getResolutionFromSize(width, height);

        result.push({ name, primary, width, height, resolution });
    }

    return result;
}

main();
