#!/usr/bin/env node

const execSync = require('child_process').execSync;

const str = execSync('i3-msg -t get_workspaces', 'utf8').toString();
const result = JSON.parse(str);
const visibleWorkspace = result.find(workspace => workspace.visible)

let target = 2;

if (visibleWorkspace && visibleWorkspace.num === 2) {
    target = 6;
}

execSync(`i3-msg workspace ${target}`);