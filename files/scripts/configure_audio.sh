#!/bin/bash

SINK_FILE="$HOME/.current_audio_sink"
DEFAULT_SINK=1

set_audio_sink() {
    pacmd set-default-source 1
    pacmd set-source-volume 1 65535
    pacmd set-default-sink $@
    echo "$@" > $SINK_FILE
}

get_audio_sink() {
    if [ -f "$SINK_FILE" ]; then
        cat $SINK_FILE
    else
        echo ""
    fi
}

if [ "$1" = "--toggle-sink" ]; then
    if [ "$(get_audio_sink)" = "1" ]; then
        set_audio_sink 0
    else
        set_audio_sink 1
    fi
else
    set_audio_sink $DEFAULT_SINK
fi
