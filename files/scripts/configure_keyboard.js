#!/usr/bin/env node

// command to detect keycode on key press: `xev`
// specify unicode character with xmodmap: `U<code>` (e.g "U1F44D")

const fs = require('fs');
const path = require('path');
const exec = require('child_process').execSync;

const LAYOUT_FILE_PATH = path.join(process.env.HOME, '/.current_layout');

function main() {
    setUsLayout();
    return;

    const switchLayout = process.argv.includes('--switch');

    if (!switchLayout) {
        setUsLayout();
    } else {
        const currentLayout = readCurrentLayout();

        if (currentLayout === 'custom') {
            setUsLayout();
        } else {
            setCustomLayout();
        }
    }
}

function setUsLayout() {
    exec('setxkbmap us intl');
    xmodmap([
        'clear lock',
        'keycode 66 = Super_L',
        'keycode 38 = a A a A agrave Agrave agrave',
        'keycode 26 = e E e E eacute Eacute eacute',
        'keycode 27 = r R r R egrave Egrave egrave',
        'keycode 30 = u U u U ugrave Ugrave ugrave',
        'keycode 31 = i I i I U1F44D', // thumb up
        'keycode 32 = o O o O U1F44C', // ok hand
        'keycode 42 = g G g G U1F3AE', // video game
        'keycode 94 = less greater',
        'keycode 48 = apostrophe quotedbl apostrophe quotedbl',
        'keycode 49 = grave asciitilde grave asciitilde dead_diaeresis'
    ]);
    setCurrentLayout('us');
}

function setCustomLayout() {
    exec('xmodmap ~/.Xmodmap')
    xmodmap([
        'clear lock',
        'keycode 66 = Super_L',
        // 'keycode 51 = dead_grave bar dead_grave bar backslash',
        // 'keycode 94 = less greater',
        // 'keycode 48 = apostrophe quotedbl apostrophe quotedbl',
        // 'keycode 49 = grave asciitilde grave asciitilde'
    ]);
    setCurrentLayout('custom');
}

function readCurrentLayout() {
    if (!fs.existsSync(LAYOUT_FILE_PATH)) {
        return '';
    } else {
        return fs.readFileSync(LAYOUT_FILE_PATH, 'utf8');
    }
}

function setCurrentLayout(value) {
    fs.writeFileSync(LAYOUT_FILE_PATH, value, 'utf8');
}

function xmodmap(cmd) {
    if (Array.isArray(cmd)) {
        for (const c of cmd) {
            xmodmap(c);
        }
    } else {
        exec(`xmodmap -e "${cmd}"`);
    }
}

main();