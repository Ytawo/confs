#!/bin/sh

FIREFOX_WID=`xdotool search --name "Mozilla Firefox" | head -1`
ACTIVE_WID=`xdotool getactivewindow`

if [ $ACTIVE_WID -ne $FIREFOX_WID ]; then
    xdotool key --window $FIREFOX_WID clearmodifiers alt+shift+p
else
    xdotool key --clearmodifiers alt+shift+p
fi
