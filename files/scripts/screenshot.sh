#!/bin/sh

if [ $1 = "--fullscreen" ] ; then
    flameshot full -p ~/Pictures
else
    flameshot gui -p ~/Pictures
fi
