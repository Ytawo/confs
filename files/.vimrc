colorscheme desert

set relativenumber
set showmatch
set shiftwidth =4
set softtabstop =4
set autoindent
set expandtab
set listchars=tab:>-,trail:-
set list
set noswapfile

autocmd FileType make setlocal noexpandtab

nnoremap ; :
nnoremap <A-Left> gT
nnoremap <A-Right> gt
nnoremap <PageUp> gT
nnoremap <PageDown> gt
inoremap <PageUp> <Esc>gTa
inoremap <PageDown> <Esc>gta
map D d$
map Y y$
map z >

map ew F>ldt<

nnoremap C :Dox<Enter>
nnoremap T :set noexpandtab<Enter>
nnoremap t :set expandtab<Enter>
nnoremap , :<Up><Up><Enter>

nnoremap ss :source ~/.vimrc<Enter>

set undofile
set undodir=~/.vim/undo
silent !mkdir -p ~/.vim/undo

set wildmenu
set wildmode=list:longest

command W :execute ':silent w !sudo tee % > /dev/null' | :edit!
