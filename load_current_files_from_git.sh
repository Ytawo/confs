#!/bin/bash

source constants.sh

# Kill xfconfd so xfce4 config files can be properly overwritten
pkill xfconfd

# Copy all config files
for file in $FILES_TO_SYNC; do
    src=$FILES_DIR/$file
    dst=~/$file
    mkdir -p $(dirname $dst)
    cp -v $src $dst
done