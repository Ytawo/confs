#!/bin/bash

source constants.sh

# Generate a SSH key
ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa -q -N ""

# Update system
sudo apt-get update
sudo apt-get upgrade -y

# Install various software
sudo apt-get install -y vim git i3 vlc g++ feh p7zip-full curl tree kolourpaint4 cmake htop gpicview xcompmgr lxappearance xdotool tumbler-plugins-extra ffmpeg unrar florence gedit

# Nvidia drivers - https://linuxconfig.org/how-to-install-the-nvidia-drivers-on-ubuntu-18-04-bionic-beaver-linux
sudo add-apt-repository -y ppa:graphics-drivers/ppa
sudo apt-get update
sudo ubuntu-drivers autoinstall

# Vulkan - https://vulkan.lunarg.com/doc/sdk/1.1.126.0/linux/getting_started_ubuntu.html
wget -qO - http://packages.lunarg.com/lunarg-signing-key-pub.asc | sudo apt-key add -
sudo wget -qO /etc/apt/sources.list.d/lunarg-vulkan-bionic.list http://packages.lunarg.com/vulkan/lunarg-vulkan-bionic.list
sudo apt update
sudo apt install -y libvulkan1 libvulkan1:i386 vulkan-sdk

# Chrome - https://www.google.com/chrome/
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O ~/Downloads/google-chrome-stable_current_amd64.deb
sudo dpkg -i ~/Downloads/google-chrome-stable_current_amd64.deb
rm ~/Downloads/google-chrome-stable_current_amd64.deb

# Rust - https://www.rust-lang.org/en-US/install.html
curl https://sh.rustup.rs -sSf > ~/Downloads/install-rust.sh
sh ~/Downloads/install-rust.sh -y
rm ~/Downloads/install-rust.sh

# Node.js - https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt-get install -y nodejs
sudo npm install -g n
sudo n latest
sudo npm install -g npm
sudo chown -R $USER:$(id -gn $USER) /home/$USER/.config

# Visual Studio Code - https://code.visualstudio.com/docs/setup/linux
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/microsoft.gpg
sudo install -o root -g root -m 644 /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install -y code
for e in rust-lang.rust bungcip.better-toml james-yu.latex-workshop geddski.macros alefragnani.numbered-bookmarks rubymaniac.vscode-paste-and-indent slevesque.shader dcasella.i3 ; do
    code --install-extension $e --force
done

# Retroarch - https://askubuntu.com/questions/952987/game-boy-advance-emulator
sudo add-apt-repository -y ppa:libretro/stable
sudo apt-get update
sudo apt-get install -y retroarch retroarch-* libretro-*

# Docker - https://docs.docker.com/engine/install/ubuntu/
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Wine - https://wiki.winehq.org/Ubuntu
sudo dpkg --add-architecture i386
wget https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/i386/libfaudio0_19.07-0~bionic_i386.deb
wget https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/amd64/libfaudio0_19.07-0~bionic_amd64.deb
sudo dpkg -i libfaudio0_19.07-0~bionic_i386.deb
sudo dpkg -i libfaudio0_19.07-0~bionic_amd64.deb
rm libfaudio0_19.07-0~bionic_i386.deb
rm libfaudio0_19.07-0~bionic_amd64.deb
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main'
rm winehq.key
sudo apt-get update
sudo apt install --install-recommends -y winehq-stable

# Lutris - https://lutris.net/downloads/
sudo add-apt-repository -y ppa:lutris-team/lutris
sudo apt-get update
sudo apt-get install -y lutris
sudo sh -c 'echo "abi.vsyscall32 = 0" >> /etc/sysctl.conf && sysctl -p'

# Discord - https://linuxconfig.org/how-to-install-discord-on-ubuntu-18-04-bionic-beaver-linux
sudo snap install discord --classic

# Flameshot - https://github.com/flameshot-org/flameshot
sudo apt install -y flameshot

# Enable autologin
cat $ROOT_DIR/other/lightdm.conf | sed "s/\$USER/$USER/" | sudo tee /etc/lightdm/lightdm.conf > /dev/null

# Update config files
bash load_current_files_from_git.sh
