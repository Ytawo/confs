#!/bin/bash

source constants.sh

for file in $FILES_TO_SYNC; do
    src=~/$file
    dst=$FILES_DIR/$file
    mkdir -p $(dirname $dst)
    cp -v $src $dst
done
